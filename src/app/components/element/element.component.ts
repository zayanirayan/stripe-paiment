import { AppConfig } from 'src/app/services/config/app-config';
import { NgForm } from '@angular/forms';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { environment } from 'src/environments/environment';
import { loadStripe } from '@stripe/stripe-js';
import { AngularStripeService } from '@fireflysemantics/angular-stripe-service';
import { MyStripeService } from 'src/app/services/stripe.service';

@Component({
  selector: 'app-element',
  templateUrl: './element.component.html',
  styleUrls: ['./element.component.css'],
})
export class ElementComponent implements OnInit {
  @ViewChild('cardInfo', { static: false }) cardInfo: ElementRef;

  stripe;
  loading = false;
  confirmation;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  stripeKey;
  constructor(
    private appConfig: AppConfig,
    private cd: ChangeDetectorRef,
    private stripeService: AngularStripeService,
    private myStripeSertvice: MyStripeService
  ) {}
  ngOnInit(): void {
    this.stripeKey = this.myStripeSertvice.getStripePublicKey();
    console.log('stripeKey', this.myStripeSertvice.getStripePublicKey());
  }

  ngAfterViewInit() {
    this.stripeService.setPublishableKey(this.stripeKey).then((stripe) => {
      this.stripe = stripe;
      const elements = stripe.elements();
      this.card = elements.create('card');
      this.card.mount(this.cardInfo.nativeElement);
      this.card.addEventListener('change', this.cardHandler);
    });
  }
  async onSubmit(form: NgForm) {
    const { token, error } = await this.stripe.createToken(this.card);

    if (error) {
      console.log('Error:', error);
    } else {
      console.log('Success!', token);
    }
  }
  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }
  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }
}

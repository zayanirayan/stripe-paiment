import { Component, Renderer2, AfterViewInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-pricing-table',
  templateUrl: './pricing-table.component.html',
  styleUrls: ['./pricing-table.component.css'],
})
export class PricingTableComponent {
  constructor(private renderer: Renderer2, private el: ElementRef) {}

  ngAfterViewInit() {
    // Create a script element for the Stripe script.
    const stripeScript = this.renderer.createElement('script');
    this.renderer.setAttribute(stripeScript, 'async', 'true');
    this.renderer.setAttribute(
      stripeScript,
      'src',
      'https://js.stripe.com/v3/pricing-table.js'
    );

    // Append the Stripe script to the component's DOM.
    this.renderer.appendChild(this.el.nativeElement, stripeScript);

    // Create a div element for the custom HTML component.
    const customComponent = this.renderer.createElement('div');
    this.renderer.appendChild(
      customComponent,
      `
      <stripe-pricing-table pricing-table-id="prctbl_1O59c5Kz8qqpz78ugY8tn60x" publishable-key="pk_test_VULVTJlEJClp1jXk0BmtfjGn">
      </stripe-pricing-table>
    `
    );

    // Append the custom component to the component's DOM.
    this.renderer.appendChild(this.el.nativeElement, customComponent);
  }
}

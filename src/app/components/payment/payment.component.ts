import { MyStripeService } from 'src/app/services/stripe.service';
import { AppConfigService } from '../../services/config/app-config.service';
import { Component, OnInit } from '@angular/core';
import { loadStripe } from '@stripe/stripe-js';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
})
export class PaymentComponent implements OnInit {
  constructor(
    private configService: AppConfigService,
    private myStripeService: MyStripeService
  ) {}
  priceId = 'price_1O1rYEKz8qqpz78uXF9lOj4M';
  product = {
    title: 'netflix premium card',
    subTitle: 'advertisement card',
    description: 'advertisement card delivered by Netflix company.',
    price: 6,
    logo: `${environment.baseHref}assets/img/netflix.jpg`,
  };
  quantity = 1;
  stripeKey: string;
  stripePromise;

  ngOnInit(): void {
    this.myStripeService.getStripePublicKey().subscribe((res) => {
      this.stripeKey = res;
      this.stripePromise = loadStripe(this.stripeKey);
    });
  }

  async checkout() {
    const stripe = await this.stripePromise;
    // @ts-ignore
    const { error } = await stripe.redirectToCheckout({
      mode: 'subscription',
      lineItems: [{ price: this.priceId, quantity: this.quantity }],
      successUrl: 'success',
      cancelUrl: 'failure',
    });
    if (error) {
      console.log(error);
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackPlatiniumComponent } from './pack-platinium.component';

describe('PackPlatiniumComponent', () => {
  let component: PackPlatiniumComponent;
  let fixture: ComponentFixture<PackPlatiniumComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PackPlatiniumComponent]
    });
    fixture = TestBed.createComponent(PackPlatiniumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

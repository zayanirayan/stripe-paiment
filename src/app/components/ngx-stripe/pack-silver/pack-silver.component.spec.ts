import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackSilverComponent } from './pack-silver.component';

describe('PackSilverComponent', () => {
  let component: PackSilverComponent;
  let fixture: ComponentFixture<PackSilverComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PackSilverComponent]
    });
    fixture = TestBed.createComponent(PackSilverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

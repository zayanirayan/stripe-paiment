import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { StripePaymentElementComponent, StripeService } from 'ngx-stripe';
import {
  StripeElements,
  StripeCardElement,
  StripeCardElementOptions,
  StripeElementsOptions,
  PaymentIntent,
} from '@stripe/stripe-js';

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from 'src/app/services/config/app-config';
import { SeoService } from 'src/app/services/seo/seo.service';
import { MyStripeService } from 'src/app/services/stripe.service';

@Component({
  selector: 'app-pack-gold',
  templateUrl: './pack-gold.component.html',
  styleUrls: ['./pack-gold.component.css'],
})
export class PackGoldComponent implements OnInit {
  @ViewChild(StripePaymentElementComponent)
  paymentElement: StripePaymentElementComponent =
    {} as StripePaymentElementComponent;
  card: StripeCardElement;
  cardOptions: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        fontWeight: '300',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0',
        },
      },
    },
  };
  amount: number = 0;

  paymentElementForm = this.fb.group({
    name: ['John doe', [Validators.required]],
    email: ['support@ngx-stripe.dev', [Validators.required]],
    address: [''],
    zipcode: [''],
    city: [''],
    // amount: [10, [Validators.required, Validators.pattern(/d+/)]],
    amount: [10, [Validators.required]],
  });

  elementsOptions: StripeElementsOptions = {
    locale: 'fr',
  } as StripeElementsOptions;

  paying = false;

  stripeTest: FormGroup;
  elements: StripeElements;
  stripeKey;

  constructor(
    private stripeService: StripeService,
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    private appConfig: AppConfig,
    private seoService: SeoService,
    private myStripeSertvice: MyStripeService
  ) {}

  ngOnInit() {
    this.myStripeSertvice.getStripePublicKey().subscribe((res) => {
      this.stripeKey = res;
      console.log('this.stripeKey 1', this.stripeKey);
      this.stripeService.changeKey(this.stripeKey);
    });
    this.seoService.generateTags({
      name: 'gold pack payment',
      content: 'this page uses stripe for this pack',
    });
    this.seoService.addTag({
      name: 'pack gold',
      content: 'stripe payment gold pack',
    });
    console.log('server url', this.appConfig.server_back_url);
    this.createPaymentIntent(1750).subscribe((pi: any) => {
      console.log('res', pi);
      console.log(
        'this.elementsOptions.clientSecret',
        this.elementsOptions.clientSecret
      );
      this.elementsOptions.clientSecret = pi.clientSecret;
      this.amount = pi.amount;
    });
  }

  pay() {
    console.log('pay clicked!');
    console.log('this.paymentElement', this.paymentElement);
    if (this.paymentElementForm.valid) {
      this.paying = true;
      this.stripeService
        .confirmPayment({
          elements: this.paymentElement.elements,
          confirmParams: {
            payment_method_data: {
              billing_details: {
                name: this.paymentElementForm.get('name').value,
                email: this.paymentElementForm.get('email').value,
                address: {
                  line1: this.paymentElementForm.get('address').value || '',
                  postal_code:
                    this.paymentElementForm.get('zipcode').value || '',
                  city: this.paymentElementForm.get('city').value || '',
                },
              },
            },
          },
          redirect: 'if_required',
        })
        .subscribe((result) => {
          this.paying = false;
          console.log('Result', result);
          if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            // alert({ success: false, error: result.error.message });
            this.router.navigateByUrl('/failure');
          } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
              // Show a success message to your customer
              // alert({ success: true });
              this.router.navigateByUrl('/success');
            }
          }
        });
    } else {
      console.log(this.paymentElementForm);
    }
  }

  createPaymentIntent(amount: number): Observable<PaymentIntent> {
    return this.http.post<PaymentIntent>(
      this.appConfig.server_back_url +
        `api/PaymentIntent/create?amount=${amount}`,
      {}
    );
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackGoldComponent } from './pack-gold.component';

describe('PackGoldComponent', () => {
  let component: PackGoldComponent;
  let fixture: ComponentFixture<PackGoldComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PackGoldComponent]
    });
    fixture = TestBed.createComponent(PackGoldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

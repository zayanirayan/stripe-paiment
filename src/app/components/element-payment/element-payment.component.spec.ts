import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementPaymentComponent } from './element-payment.component';

describe('ElementPaymentComponent', () => {
  let component: ElementPaymentComponent;
  let fixture: ComponentFixture<ElementPaymentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ElementPaymentComponent]
    });
    fixture = TestBed.createComponent(ElementPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

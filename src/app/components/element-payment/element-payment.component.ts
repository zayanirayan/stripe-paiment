import { Component } from '@angular/core';
import { AppConfigService } from 'src/app/services/config/app-config.service';
import { MyStripeService } from 'src/app/services/stripe.service';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-element-payment',
  templateUrl: './element-payment.component.html',
  styleUrls: ['./element-payment.component.css'],
})
export class ElementPaymentComponent {
  paymentHandler: any = null;
  stripeKey;

  constructor(
    private configService: AppConfigService,
    private myStripeService: MyStripeService
  ) {}
  ngOnInit() {
    this.myStripeService.getStripePublicKey().subscribe((res) => {
      this.stripeKey = res;
    });
    this.invokeStripe();
  }
  makePayment(amount: any) {
    const paymentHandler = (<any>window).StripeCheckout.configure({
      key: this.stripeKey,
      locale: 'auto',
      token: function (stripeToken: any) {
        console.log(stripeToken);
        alert('Stripe token generated!');
      },
    });
    paymentHandler.open({
      name: 'netflix premium card',
      description: 'advertisement card delivered by Netflix company.',
      amount: amount * 100,
    });
  }
  invokeStripe() {
    if (!window.document.getElementById('stripe-script')) {
      const script = window.document.createElement('script');
      script.id = 'stripe-script';
      script.type = 'text/javascript';
      script.src = 'https://checkout.stripe.com/checkout.js';
      script.onload = () => {
        this.paymentHandler = (<any>window).StripeCheckout.configure({
          key: this.stripeKey,
          locale: 'auto',
          token: function (stripeToken: any) {
            console.log(stripeToken);
            alert('Payment has been successfull!');
          },
        });
      };

      window.document.body.appendChild(script);
    }
  }
}

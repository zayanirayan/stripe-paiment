import { AppConfigService } from './services/config/app-config.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'stripeApp';
  constructor(private appConfig: AppConfigService) {}
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PaymentComponent } from './components/payment/payment.component';
import { SuccessComponent } from './components/payment/success/success.component';
import { FailureComponent } from './components/payment/failure/failure.component';
import { ElementPaymentComponent } from './components/element-payment/element-payment.component';
import { ElementComponent } from './components/element/element.component';
import { NgxStripeComponent } from './components/ngx-stripe/ngx-stripe.component';
import { PricingTableComponent } from './components/pricing-table/pricing-table.component';
import { CardElementComponent } from './components/ngx-stripe/card-element/card-element.component';
import { PackSilverComponent } from './components/ngx-stripe/pack-silver/pack-silver.component';
import { PackGoldComponent } from './components/ngx-stripe/pack-gold/pack-gold.component';
import { PackPlatiniumComponent } from './components/ngx-stripe/pack-platinium/pack-platinium.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'elt-payment', component: ElementPaymentComponent },
  { path: 'element', component: ElementComponent },
  { path: 'ngx-stripe', component: NgxStripeComponent },
  { path: 'ngx-card', component: CardElementComponent },
  { path: 'pricing', component: PricingTableComponent },
  { path: 'success', component: SuccessComponent },
  { path: 'failure', component: FailureComponent },
  { path: 'silver-pack', component: PackSilverComponent },
  { path: 'gold-pack', component: PackGoldComponent },
  { path: 'platinium-pack', component: PackPlatiniumComponent },
  { path: '**', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root',
})
export class SeoService {
  constructor(private meta: Meta) {}

  generateTags(config) {
    config = {
      title: 'something',
      description: 'meta description',
      image: 'link to image',
      slug: '',
      ...config,
    };

    this.meta.updateTag({ name: 'myrsi:site', content: 'site web myrsi' });
    this.meta.updateTag({
      name: 'myrsi:description',
      content: 'this website supports online payment',
    });

    this.meta.updateTag({
      property: 'myrsi:title',
      content: 'myrsi.fr/stripe',
    });
    this.meta.updateTag({
      property: 'myrsi:description',
      content: 'mayar des services informatique',
    });
  }

  addTag(config) {
    config = {
      name: 'something',
      content: 'meta description',
      ...config,
    };
    this.meta.addTag(config);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from './app-config';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AppConfigService extends AppConfig {
  configFile = `${environment.baseHref}config/config.json`;
  constructor(private http: HttpClient) {
    super();
  }

  loadConfig() {
    return this.http
      .get<AppConfig>(this.configFile)
      .toPromise()
      .then((data) => {
        this.server_back_url = data.server_back_url;
      })
      .catch(() => {
        console.error('Could not load configuration!');
      });
  }
}

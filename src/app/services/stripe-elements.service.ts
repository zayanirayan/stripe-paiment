import { Injectable } from '@angular/core';
import {
  Stripe,
  StripeElements,
  StripeElementsOptions,
  loadStripe,
} from '@stripe/stripe-js';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StripeElementsService {
  stripe: Stripe | undefined;
  elements: StripeElements | undefined;

  constructor() {
    this.initializeStripe();
  }

  async initializeStripe() {
    try {
      this.stripe = await loadStripe(environment.stripe.publicKey);

      if (this.stripe) {
        const elementsOptions: StripeElementsOptions = {
          fonts: [
            {
              cssSrc: 'https://fonts.googleapis.com/css?family=Roboto',
            },
          ],
        };
        // this.elements = this.stripe.elements(elementsOptions);
      } else {
        console.error('Stripe was not properly initialized.');
      }
    } catch (error) {
      console.error('Error initializing Stripe:', error);
    }
  }
}

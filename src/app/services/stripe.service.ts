import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AppConfig } from './config/app-config';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MyStripeService {
  key: string;
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  checkout(priceId: string, quantity: number) {
    const body = {
      priceId,
      quantity,
      successUrl: environment.stripe.successUrl,
      cancelUrl: environment.stripe.failureUrl,
    };
    this.getStripePublicKey().subscribe((res) => {
      this.key = res;
    });
    return this.http.post(this.key, body);
  }

  /**
   * get stripeKey from myrsi backend server
   */
  getStripePublicKey(): Observable<string> {
    let key;
    return this.http.get(
      this.appConfig.server_back_url + `api/PaymentIntent/cle-publique`,
      {
        responseType: 'text',
      }
    );
    // .subscribe((res) => {
    //   console.log('res', res);
    //   key = res.toString();
    // });
    // return key;
  }
}

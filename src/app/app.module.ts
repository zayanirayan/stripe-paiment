import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { PaymentComponent } from './components/payment/payment.component';
import { SuccessComponent } from './components/payment/success/success.component';
import { FailureComponent } from './components/payment/failure/failure.component';
import { ElementPaymentComponent } from './components/element-payment/element-payment.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ElementComponent } from './components/element/element.component';
import { environment } from 'src/environments/environment';
import { AppConfigService } from './services/config/app-config.service';
import { AppConfig } from './services/config/app-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxStripeComponent } from './components/ngx-stripe/ngx-stripe.component';
import { NgxStripeModule } from 'ngx-stripe';
import { PricingTableComponent } from './components/pricing-table/pricing-table.component';
import { CardElementComponent } from './components/ngx-stripe/card-element/card-element.component';
import { PackSilverComponent } from './components/ngx-stripe/pack-silver/pack-silver.component';
import { PackGoldComponent } from './components/ngx-stripe/pack-gold/pack-gold.component';
import { PackPlatiniumComponent } from './components/ngx-stripe/pack-platinium/pack-platinium.component';

export function initializerFn(appConfigService: AppConfigService) {
  return () => {
    return appConfigService.loadConfig();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PaymentComponent,
    SuccessComponent,
    FailureComponent,
    ElementPaymentComponent,
    ElementComponent,
    NgxStripeComponent,
    PricingTableComponent,
    CardElementComponent,
    PackSilverComponent,
    PackGoldComponent,
    PackPlatiniumComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxStripeModule.forRoot(''),
  ],
  providers: [
    {
      provide: AppConfig,
      deps: [HttpClient],
      useExisting: AppConfigService,
    },
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: initializerFn,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

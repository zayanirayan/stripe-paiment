export const environment = {
  baseHref: 'https://myrsi.fr/stripe/',
  production: true,
  stripe: {
    successUrl: 'https://myrsi.fr/stripe/success',
    failureUrl: 'https://myrsi.fr/stripe/failure',
  },
};

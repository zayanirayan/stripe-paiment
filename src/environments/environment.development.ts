export const environment = {
  baseHref: 'http://localhost:4200/',
  production: false,
  stripe: {
    successUrl: 'http://localhost:4200/success',
    failureUrl: 'http://localhost:4200/failure',
  },
};
